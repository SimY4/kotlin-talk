package kotlinInAction.basics

interface I {

    var property: Any                                        // <- a field with getter and setter

    fun method1(arg: Any): Any                              // <- regular method

    fun method2(arg: Any) = "Can contain implementation"

}





class C(override var property: Any): I { // <- classes are final by default

    // static members not allowed

    lateinit var lateInitProperty: String // <- Property promised to be initialized later but before the first use

    override fun method1(arg: Any) = TODO("A way to represent not implemented code paths") // <- ??? in scala

    fun method3(str: String = "nothing"): String = TODO("Method with default arguments")

//                                         \/ Can be omitted
    val delegatingProperty: String by lazy(LazyThreadSafetyMode.PUBLICATION) { "Property with initialization delegate" }

//  + - / * % .. ++ -- == != += -= /= *= %= unary + - ! supported for overloading
    operator fun plus(c: C): C = TODO("Limited support for operator overloading")

    infix fun map(f: (Any) -> Any): Any = TODO("infix functions can be invoked like operators")

    infix fun `|+|`(other: C): C = TODO("crazy syntax has to be wrapped in `")

}

val c1: C = C("constructor call") // <- no new keyword
val c2: C = C("constructor call")

val c3 = c1 + c2
val a = c1 map { i -> i }
val cc = c1 `|+|` c2







interface A { fun a(): Int }
class IA: A { override fun a() = 1 }

interface B { fun b(): Int }
class IB: B { override fun b() = 2 }

interface ABFacade: A, B
class ABDelegatingFacade: ABFacade, A by IA(), B by IB() // <- delegating implementation







data class Entity(val field1: Any, val field2: Any = "default value") // <- similar to case classes in Scala

val e = Entity("a", "b")
val newE = e.copy(field2 = "c") // <- parameters can be referred by name


/**
 * Language support for singleton types
 */
object Singleton {

    inline fun inlineFunction(f: (Any) -> Any): Any = TODO("Functions can be inlined in callsite")

    inline fun partialInline(f1: (Any) -> Any, noinline f2: (Any) -> Any): Any = TODO()

    fun inlineExample(list: List<Int>): Boolean {
        list.forEach { i ->
            if (0 == i % 2) {
                return true // <- this return will break out the loop and exit inlineExample
            }
        }

        list.forEach { i ->
            if (0 == i % 3) {
                return@forEach // <- this return will break out the loop only
            }
        }
        return false
    }





    fun destructuringExample() {
        val e = Entity(1, 2)
        val (a, b) = e
    }

}
