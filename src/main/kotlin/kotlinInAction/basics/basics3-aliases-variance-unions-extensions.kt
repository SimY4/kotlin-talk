package kotlinInAction.basics

import java.io.Serializable
import java.math.BigInteger as BI

typealias Reader<T> = (String) -> T

typealias Identifier = String






interface Variance<in I, out O>

interface RestrictedGenerics<in U> where U: Serializable, U: Comparable<U>






/**
 * Sealed unions.
 */
sealed class Option<out T>

data class Some<out T>(val get: T): Option<T>()

object None: Option<Nothing>()

fun <T> sealedUnions(t: Option<T>): T? = when(t) {
    is Some -> t.get // <- implicit casting to Some
    is None -> null
// no else statement since we know that there could be only Some and None
}







fun <T> option(o: T?): Option<T> = if (o == null) None else Some(o) // <- implicit casting to non-nullable o

/** extension function for the same thing */
fun <T> T?.opt(): Option<T> = option(this) // <- implicit casting to non-nullable o

/** extension synthetic property */
val Long.bi: BI get() = BI.valueOf(this) // just like scala implicit conversions

object Extensions {
    val some: Option<Int> = 2.opt()
    val none: Option<Int> = null.opt()

    val bi: BI = 123L.bi

    // Some built-in extensions

    val any: Int? = null
    val let: Int? = any?.let { it + 1 } // similar to .map { it + 1 }

    val takeIf: Int? = 1.takeIf { it >= 0 } // similar to .filter { it >= 0 }

    val apply: List<Int> = arrayListOf<Int>().apply {
        add(0)
        add(1)
        add(2)
    }

    val with: List<Int> = with(arrayListOf<Int>()) {
        add(0)
        add(1)
        add(2)
        this
    }

    val run: List<Int> = arrayListOf<Int>().run {
        add(0)
        add(1)
        add(2)
        this
    }

}






/**
 * Lambdas with implicit receiver A.() -> B
 */
object FunctionsWithImplicitReceiver {
    class Head {
        var title: String? = null
    }

    open class Div {
        var text: String? = null
        var divs: MutableList<Div> = arrayListOf()

        fun div(div: Div.() -> Unit) {
            val d = Div()
            div(d)
            this.divs.add(d)
        }
    }

    class Body: Div()

    class Html {
        var head: Head? = null
        var body: Body? = null

        fun head(head: Head.() -> Unit) {
            val h = Head()
            head(h)
            this.head = h
        }

        fun body(body: Body.() -> Unit) {
            val b = Body()
            body(b)
            this.body = b
        }
    }

    fun html(html: Html.() -> Unit): Html {
        val h = Html()
        html(h)
        return h
    }

    private val html =
            html {
                head {
                    title = "Kotlin is awesome"
                }
                body {
                    div {
                        text = "Hello world"
                    }
                }
            }

}









// WAT

val whatAmI = null + null
