package kotlinInAction.basics

// Kotlin types

// ! no top level class definition

val byte        : Byte    = 0xf // <- no semicolons
val integer     : Int     = 1
val long        : Long    = 1L
val double      : Double  = 1.0
val char        : Char    = 'a'
val string      : String  = "Hello, world"

val array       : Array<Int>        = arrayOf(1, 2, 3)
val list        : List<Int>         = listOf(1, 2, 3)
val set         : Set<Int>          = setOf(1, 2, 3)
val pair        : Pair<Int, String> = 1 to "string"
val map         : Map<Int, String>  = mapOf(pair, pair, pair)
val closedRange : IntRange          = 1 .. 10
val range       : IntRange          = 1 until 10
val progression : IntProgression    = 10 downTo 1






val i: Int  = 1
val l: Long = i // <- implicit transformations not allowed







val any         : Any     = "Supertype of all types"
val unit        : Unit    = Unit // <- void type
val nothing     : Nothing = TODO("Type that has no instances")






//                                        \/ no return statement inside function literals
val supplier    : () -> String         = { "function literal with 0 args" }
val function    : (Any, Any) -> String = { a, b -> "function literal with 2 args" }







val anotherString   = "Another string" // <- type information can be inferred
val stringInference = "String inference is here $anotherString"  // <- string inference







// First class support for nullable types

val nullableType: Any? = null               // <- nullable type

val a1: Any = nullableType                  // <- not allowed to assign to a non nullable type
val a2: Any = nullableType ?: "alternative" // <- unless default value is specified (elvis operator)
val a3: Any = nullableType!!                // <- or forced

val nullableA: Any? = a1                    // <- allowed to assign non nullable to nullable at any moment

val nullableListOfAny: List<Any>?
val ListOfNullableAny: List<Any?>
val nullableListOfNullableAny: List<Any?>?


val nullableArgumentFunction: (Any?) -> Int = { nullable -> when {
    nullable == null -> -1
    nullable is String -> nullable.length // <- auto casting to non-nullable string type if proven by code that the type is String
} }
