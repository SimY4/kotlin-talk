package kotlinInAction

const val intro = """





                          ## Kotlin in Action:

                          - Basics
                              - Primitives and Literals
                              - Interfaces, Classes, Functions
                              - Properties, Delegates
                              - Type Aliases
                              - Generic Variance
                              - Sealed Unions
                              - Extension Functions
                              - WAT

                          - Coroutines
                              - Suspended computations
                              - Async/await






    """
