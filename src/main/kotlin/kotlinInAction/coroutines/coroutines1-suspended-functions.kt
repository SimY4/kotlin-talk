package kotlinInAction.coroutines

import kotlinx.coroutines.*

suspend fun doSomething(a: Any): Any = TODO("Suspended functions")
suspend fun doSomethingElse(a: Any): Any = TODO("Suspended functions")

object Coroutines {
    val res1: Any = doSomething(Any()) // Not allowed to call suspended functions in synchronous context

    val res2: Any = runBlocking { doSomething(Any()) } // calling inside async blocks is fine

    val job: Job = runBlocking {
        launch { doSomething(Any()) } // calling offthread without waiting for result

        //...
    }

    // Chain async calls to get back deferred result immediately
    val async: Deferred<Any> = runBlocking {
        async {
            val suspendedResult = doSomething(Any())

            fun innerFunction() = doSomething(Any()) // Not allowed to suspend in deep nested code - only within coroutine body
            innerFunction()

            doSomethingElse(suspendedResult)
        }
    }

}
